function convertMcdos()
{
  console.log("function called");
  var mcdo = 9;
  var input = document.getElementById('budget').value;
  var result = input /mcdo;
  console.log("input is   " + input);
  var res = result.toPrecision(5);
  document.getElementById("mcdo").innerHTML = res;
}
function validate()
{
  budget = document.getElementById('budget').value;
  people = document.getElementById('people').value;
  departure = document.getElementById('date-departure').value;
  arrival = document.getElementById('date-arrival').value;
  debug = document.getElementById('debug')
  var travel = getSelectedResults(document.getElementById('travels'));
  var countries =  getSelectedResults(document.getElementById('countries'))
  if (budget == null || people == null|| departure == null|| arrival == null)
    {
      debug.style.color = "red"
      debug.innerHTML = "One of the field is not filled"
    }
  debug.innerHTML  = "Un voyage de "  + people + " personnes  avec un budget de "
   + budget + " euros du " + departure + " au " + arrival + " souhaitant voyager soit " + travel + " et preferant les pays suivants " + countries
}
function getSelectedResults(select)
{
  var result = [];
   var options = select && select.options;
   var opt;
   for (var i=0, iLen=options.length; i<iLen; i++) {
     opt = options[i];
     if (opt.selected) {
       result.push(opt.value || opt.text);
     }
   }
   return result;
}
function getCountries() {
  jQuery.when(
      jQuery.getJSON('./datas/countriesToCities.json')
  ).done( function(json) {
      console.log(json);
      fill_select(json);
  });
}
function fill_select(countries) {
  var listItems = {};
  console.log(countries);
  var objKeys = Object.keys(countries);
  console.log(objKeys);
  objKeys.sort();
  var sel = document.getElementById('countries');
  for (var i = 0; i < objKeys.length; i++) {
               listItems += "<option value='" + objKeys[i] + "'>" + objKeys[i] + "</option>";
           }
           $("#countries").html(listItems);
}
function initMap() {
        var uluru = {lat: -25.363, lng: 131.044};
        var map = new google.maps.Map(document.getElementById('map'), {
          zoom: 4,
          center: uluru
        });
        var marker = new google.maps.Marker({
          position: uluru,
          map: map
        });
      }
$(function(){
    $('[type="date"].min-today').prop('min', function(){
        return new Date().toJSON().split('T')[0];
    });
});
